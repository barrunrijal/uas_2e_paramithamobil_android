package novan.seka.apkmobil

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_data_merk.*
import kotlinx.android.synthetic.main.merk.*
import kotlinx.android.synthetic.main.merk.*

class AdapterMerk(val datamerk : List<HashMap<String,String>>,val pkt : merkActivity) : RecyclerView.Adapter<AdapterMerk.HolderMerk> () {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterMerk.HolderMerk {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_data_merk,parent,false)
        return HolderMerk(v)
    }

  /*  override fun getItemCount(): Int {
        return datamerk.size
    }

    override fun onBindViewHolder(holder: AdapterMerk.HolderMerk, position: Int) {
        val data =datamerk.get(position)
        holder.txmerk.setText(data.get("merk"))
        holder.txnama.setText(data.get("nama_mobil"))
        if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(230,245,240))
        else holder.layouts.setBackgroundColor(Color.rgb(255,255,245))

        holder.layouts.setOnClickListener(View.OnClickListener {
            pkt.edMerk.setText(data.get("merk"))
            pkt.edNama_Mobil.setText(data.get("nama_mobil"))

        })


    }
*/
    class HolderMerk(v : View) :  RecyclerView.ViewHolder(v){
        class notifyDataSetChanged {
            fun notifyDataSetChanged() {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        }

        //  val prod_id = v.findViewById<TextView>(R.id.pro_id)
        val txmerk = v.findViewById<TextView>(R.id.txMerk)
        val txnama = v.findViewById<TextView>(R.id.txNama)
        val layouts = v.findViewById<ConstraintLayout>(R.id.paketLayout)
    }

}