package novan.seka.apkmobil

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btMobil.setOnClickListener(this)
        btMerk.setOnClickListener(this)
        btBckMain.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btMobil->{
                val intent = Intent(this,mobilActivity::class.java)
                startActivity(intent)
                finish()
            }
            R.id.btMerk -> {
                val intent = Intent(this,merkActivity::class.java)
                startActivity(intent)
                finish()
            }
            R.id.btBckMain -> {
                val intent = Intent(this,loginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }
}
