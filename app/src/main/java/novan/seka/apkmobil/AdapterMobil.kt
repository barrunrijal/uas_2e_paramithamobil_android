package novan.seka.apkmobil

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.mobil.*


class AdapterMobil(val dataMobil: List<HashMap<String,String>>, val mbl1: mobilActivity): RecyclerView.Adapter<AdapterMobil.HolderDataMobil> (){
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterMobil.HolderDataMobil {
        val v  =LayoutInflater.from(parent.context).inflate(R.layout.item_data_mobil,parent,false)
        return HolderDataMobil(v)
    }

    override fun getItemCount(): Int {
        return dataMobil.size
    }

    override fun onBindViewHolder(holder: AdapterMobil.HolderDataMobil, position: Int) {
        val data =dataMobil.get(position)
        holder.nama.setText(data.get("nama"))
        holder.harga.setText(data.get("harga"))
        holder.tahun.setText(data.get("tahun"))
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.images)

        if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(230,245,240))
        else holder.layouts.setBackgroundColor(Color.rgb(255,255,245))

        holder.layouts.setOnClickListener(View.OnClickListener{
            mbl1.edNamaAd.setText(data.get("nama"))
            mbl1.edHargaAd.setText(data.get("harga"))
            mbl1.edTahunAd.setText(data.get("tahun"))
            mbl1.id_mobil = data.get("id_mobil").toString()
            Picasso.get().load(data.get("url")).into(mbl1.img1)

        })
    }

    class HolderDataMobil(v : View) :  RecyclerView.ViewHolder(v){
        val nama = v.findViewById<TextView>(R.id.txNama)
        val harga = v.findViewById<TextView>(R.id.txHarga)
        val tahun = v.findViewById<TextView>(R.id.txTahun)
        val images = v.findViewById<ImageView>(R.id.img)
        val layouts = v.findViewById<ConstraintLayout>(R.id.muridLayout)
    }

}