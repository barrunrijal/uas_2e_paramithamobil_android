package novan.seka.apkmobil

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.mobil.*
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.HashMap

class mobilActivity : AppCompatActivity(), View.OnClickListener {

    var daftarmobil = mutableListOf<HashMap<String,String>>()
    var id_mobil : String =""
    var imStr = ""
    var namafile = ""
    var Fileuri = Uri.parse("")
    lateinit var AdapterMobil : AdapterMobil
    lateinit var  MediaHelperKamera : MediaHelperKamera
    var uri = "http://192.168.43.117/mobil/show_data.php"
    var uri1 = "http://192.168.43.117/mobil/query_data.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.mobil)
        AdapterMobil =  AdapterMobil(daftarmobil,this)
        rv1.layoutManager = LinearLayoutManager(this)
        rv1.adapter = AdapterMobil
        img1.setOnClickListener(this)
        btInsMobil.setOnClickListener(this)
        btUpdtMobil.setOnClickListener(this)
        btDelMobil.setOnClickListener(this)
        btBckM.setOnClickListener(this)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        MediaHelperKamera = MediaHelperKamera()
    }

    override fun onStart() {
        super.onStart()
        ShowMobil()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){

            if (requestCode == MediaHelperKamera.getRcCamera()){
                imStr = MediaHelperKamera.getBitmapToString(img1,Fileuri)
                namafile = MediaHelperKamera.getMyfile()
            }
        }
    }

    fun ShowMobil(){
        val request = StringRequest(Request.Method.POST,uri,Response.Listener { response ->
            daftarmobil.clear()
            val jsonArray = JSONArray(response)

            for (x in 0..(jsonArray.length()-1)){
                val jsonObject = jsonArray.getJSONObject(x)
                var murd = HashMap<String,String>()
                murd.put("nama",jsonObject.getString("nama_mobil"))
                murd.put("harga",jsonObject.getString("harga"))
                murd.put("tahun",jsonObject.getString("tahun"))
                murd.put("url",jsonObject.getString("url"))
                murd.put("id_mobil",jsonObject.getInt("id_mobil").toString())
                daftarmobil.add(murd)
            }
            AdapterMobil.notifyDataSetChanged()
        },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi",Toast.LENGTH_SHORT).show()
            })

        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.img1->{
                requestPermition()
            }
            R.id.btInsMobil->{
                query("insert")
            }
            R.id.btUpdtMobil->{
                query("update")
            }
            R.id.btDelMobil->{
                query("delete")
            }
            R.id.btBckM ->{
                val intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    fun query(mode : String){
        val request = object : StringRequest(Method.POST,uri1,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil Ditambahkan", Toast.LENGTH_LONG).show()
                    ShowMobil()
                }else{
                    Toast.makeText(this,"Operasi Gagal Ditambahkan", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nama",edNamaAd.text.toString())
                        hm.put("harga",edHargaAd.text.toString())
                        hm.put("tahun",edTahunAd.text.toString())
                        hm.put("images",imStr)
                        hm.put("file",namafile)
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("nama",edNamaAd.text.toString())
                        hm.put("harga",edHargaAd.text.toString())
                        hm.put("tahun",edTahunAd.text.toString())
                        hm.put("images",imStr)
                        hm.put("file",namafile)
                        hm.put("id_mobil",id_mobil)
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("id_mobil",id_mobil)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun requestPermition() = runWithPermissions(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        Fileuri = MediaHelperKamera.getOutputFileUri()

        val inten  = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        inten.putExtra(MediaStore.EXTRA_OUTPUT,Fileuri)
        startActivityForResult(inten,MediaHelperKamera.getRcCamera())
    }
}