package novan.seka.apkmobil

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.user_main.*
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.HashMap

class UserMain : AppCompatActivity(), View.OnClickListener {

    var imStr = ""
    var namafile = ""
    var Fileuri = Uri.parse("")
    lateinit var  MediaHelperKamera : MediaHelperKamera
    var uri1 = "http://192.168.43.36/bimbel/query_data.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_main)
        imVreg.setOnClickListener(this)
        btnReg1.setOnClickListener(this)
        btnKem.setOnClickListener(this)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        MediaHelperKamera = MediaHelperKamera()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){

            if (requestCode == MediaHelperKamera.getRcCamera()){
                imStr = MediaHelperKamera.getBitmapToString(imVreg,Fileuri)
                namafile = MediaHelperKamera.getMyfile()
            }
        }
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imVreg->{
                requestPermition()
            }
            R.id.btnReg1->{
                query("insert")
            }
            R.id.btnKem ->{
                val intent = Intent(this,loginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    fun query(mode : String){
        val request = object : StringRequest(Method.POST,uri1,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nama",edNamaUser.text.toString())
                        hm.put("no_hp",edNoHpUser.text.toString())
                        hm.put("alamat",edAlmtUser.text.toString())
                        hm.put("images",imStr)
                        hm.put("file",namafile)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun requestPermition() = runWithPermissions(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        Fileuri = MediaHelperKamera.getOutputFileUri()

        val inten  = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        inten.putExtra(MediaStore.EXTRA_OUTPUT,Fileuri)
        startActivityForResult(inten,MediaHelperKamera.getRcCamera())
    }
}