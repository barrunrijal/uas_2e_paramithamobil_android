package novan.seka.apkmobil

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.merk.*
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.HashMap

class merkActivity : AppCompatActivity() , View.OnClickListener {

    lateinit var adapMerk: AdapterMerk
    var id_merk : String =""
    var daftarmerk = mutableListOf<HashMap<String,String>>()
    var uri1 = "http://192.168.43.117/mobil/show_paket.php"
    var uri2 = "http://192.168.43.117/mobil/query_paket.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.merk)
        adapMerk =  AdapterMerk(daftarmerk,this)
        rv2.layoutManager = LinearLayoutManager(this)
        rv2.adapter = adapMerk

        btInsMerk.setOnClickListener(this)
        btUpdtMerk.setOnClickListener(this)
        btDelMerk.setOnClickListener(this)
        btBckP.setOnClickListener(this)

    }

    override fun onStart() {
        super.onStart()
        Showmerk()
    }


    fun Showmerk(){
        val request = StringRequest(Request.Method.POST,uri1, Response.Listener { response ->
            daftarmerk.clear()
            adapMerk.notifyDataSetChanged()
            val jsonArray = JSONArray(response)
            for (x in 0..(jsonArray.length()-1)){
                val jsonObject = jsonArray.getJSONObject(x)
                var paket1 = HashMap<String,String>()
                paket1.put("id_merk",jsonObject.getString("id_merk"))
                paket1.put("nama_mobil",jsonObject.getString("nama_mobil"))
                daftarmerk.add(paket1)

            }
            adapMerk.notifyDataSetChanged()

        }, Response.ErrorListener { error ->
            Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
        })
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }



    fun query(mode : String){
        val request = object : StringRequest(Method.POST,uri2,
            Response.Listener { response ->

                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil Ditambakan", Toast.LENGTH_LONG).show()
                    Showmerk()
                }else{
                    Toast.makeText(this,"Operasi Gagal Ditambahkan", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            })
        {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("id_merk",edMerk.text.toString())
                        hm.put("nama_mobil",edNama_Mobil.text.toString())
                    }

                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("id_merk",edMerk.text.toString())
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("id_merk",edMerk.text.toString())
                        hm.put("nama_mobil",edNama_Mobil.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btInsMerk->{
                query("insert")



            }
            R.id.btDelMerk->{
                query("delete")


            }
            R.id.btUpdtMerk->{
                query("update")
            }
            R.id.btBckP->{
                val intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }
}